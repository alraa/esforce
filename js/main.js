$(document).ready(function() {
	
	//placeholder
	$('[placeholder]').focus(function() {
		var input = $(this);
		if(input.val() == input.attr('placeholder')) {
			input.val('');
			input.removeClass('placeholder');
		}
	}).blur(function() {
		var input = $(this);
		if(input.val() == '' || input.val() == input.attr('placeholder')) {
			input.addClass('placeholder');
			input.val(input.attr('placeholder'));
		}
	}).blur();
	$('[placeholder]').parents('form').submit(function() {
		$(this).find('[placeholder]').each(function() {
			var input = $(this);
			if(input.val() == input.attr('placeholder')) {
				input.val('');
			}
		})
	});

	//open hidden parameters
	$('.link-opener').click(function() {
		$(this).parents('.pas-table').find('tr').removeClass('hid');
		$(this).parents('tr').addClass('hid');		
		return false;
	});

	//fancybox window
	$('.fancy').fancybox();

	//choose project in header
	$('.proj-list li a').on('click', function() {
		$(this).parents('.proj-list').find('li').addClass('dis').removeClass('active');
		$(this).parent('li').removeClass('dis').addClass('active');
		$(this).parents('.proj-list').next('.btn-remove').addClass('active');		
		var numb = $(this).parents('.proj-list').find('ul').find('li.active').length;
			if(numb > 0) {
				$(this).parents('.proj-list').find('.ac-num').text( '+'+numb);
			} else { 
				$(this).parents('.proj-list').find('.ac-num').text( '...');
			} 
		setTimeout(function () {}, 50);
		return false;
	});

	//clear choosen project
	$('.proj-row .btn-remove').on('click', function() {
		$(this).parents('.proj-row').find('li').removeClass('dis').addClass('active');
		var numb = $(this).prev('.proj-list').find('ul').find('li.active').length;
			if(numb > 0) {
				$(this).prev('.proj-list').find('.ac-num').text( '+'+numb);
			} else { 
				$(this).prev('.proj-list').find('.ac-num').text( '...');
			} 
		$(this).removeClass('active');
		return false;
	});
	$('.proj-row.default .btn-remove').on('click', function() {
		$(this).parents('.proj-row').remove();
		var nmb = $('.proj-row').length;
		if(nmb > 2) {
			$('.head-cntr').addClass('nmb');
		} else { 

			$('.head-cntr').removeClass('nmb');
			var numb = $('.proj-row.vs .proj-list').find('ul').find('li.active').length-1;
			if(numb > 0) {
				$('.proj-row.vs .proj-list').find('.ac-num').text( '+'+numb);
			} else { 
				$('.proj-row.vs .proj-list').find('.ac-num').text( '...');
			} 

		} 
		return false;
	});

	//add project row in header
	$('.proj-row .btn-add').on('click', function() {
		$('.proj-row.default:first-child').clone(true).appendTo('.head-cntr');
		$(this).addClass('hid');
		$('.head-cntr').addClass('nmb');
		$('.ac-num').text( '+5');
		return false;
	});


	//datepicker
	$.extend($.datepicker,{_checkOffset:function(inst,offset,isFixed){return offset}});
	$.datepicker.setDefaults( $.datepicker.regional[ "ru" ] );
	$("#datepicker").datepicker({
		changeMonth: true,
		changeYear: true,
    });
    $(".period-inp input").datepicker({
		changeMonth: true,
		changeYear: true,
		maxDate: new Date,
		onChangeMonthYear: function () {
		setTimeout(function() {
			$('.ui-datepicker select').styler();
		}, 2);
		}
    });
    $(".period-inp input").click(function(){
    	$('.ui-datepicker select').styler();
    });

	
	//period-link1
	$('.period-link1').on('click', function() {
		$(this).addClass('active');
		$(this).parents('.period-row').find('.period-link').not(this).removeClass('active');
		$('#date2').datepicker().datepicker("setDate", new Date() );
		$('#date1').datepicker().datepicker("setDate", -7);
		return false;
	});
	//period-link2
	$('.period-link2').on('click', function() {
		$(this).addClass('active');
		$(this).parents('.period-row').find('.period-link').not(this).removeClass('active');
		$('#date2').datepicker().datepicker("setDate", new Date());
		$('#date1').datepicker().datepicker("setDate", '-1m');
		return false;
	});

	//add period line
	$('.period-row .btn-add').on('click', function() {
		$(this).parent('.period-row').next('.period-row').removeClass('hid');
		$(this).addClass('hid');
		return false;

		//period-link3
		$('.period-link3').on('click', function() {
			$(this).addClass('active');
			$(this).parents('.period-row').find('.period-link').not(this).removeClass('active');
			$('#date4').datepicker().datepicker("setDate", new Date() );
			$('#date3').datepicker().datepicker("setDate", -7);
			return false;
		});
		//period-link4
		$('.period-link4').on('click', function() {
			$(this).addClass('active');
			$(this).parents('.period-row').find('.period-link').not(this).removeClass('active');
			$('#date4').datepicker().datepicker("setDate", new Date());
			$('#date3').datepicker().datepicker("setDate", '-1m');
			return false;
		});
		
	});
	//remove period row
	$('.period-row .btn-remove').on('click', function() {
		$(this).parent('.period-row').addClass('hid');
		$('.period-row .btn-add').removeClass('hid');
		return false;
	});



	// Hide Header on on scroll down
	var didScroll;
	var lastScrollTop = 0;
	var delta = 5;
	var navbarHeight = $('#header').outerHeight();

	$(window).scroll(function(event){
	    didScroll = true;
	});

	setInterval(function() {
	    if (didScroll) {
	        hasScrolled();
	        didScroll = false;
	    }
	}, 250);

	function hasScrolled() {
	    var st = $(this).scrollTop();
	    
	    // Make sure they scroll more than delta
	    if(Math.abs(lastScrollTop - st) <= delta)
	        return;
	    
	    // If they scrolled down and are past the navbar, add class .nav-up.
	    // This is necessary so you never see what is "behind" the navbar.
	    if (st > lastScrollTop && st > 0){
	        // Scroll Down
	        $('#header').removeClass('nav-down').addClass('nav-up');
	        $('#ui-datepicker-div').addClass('nav-up');
	    }else {
	        // Scroll Up
	        if(st + $(window).height() < $(document).height()) {
	            $('#header').removeClass('nav-up').addClass('nav-down');
	            $('#ui-datepicker-div').removeClass('nav-up');
	        }
	    }	    
	    lastScrollTop = st;
	}

	//fix-header
	function showDiv() {
		if ($(window).scrollTop() >= 100 && $('#header').data('positioned') == 'false') {
			$("#header").data('positioned', 'true').addClass('fix');
		} else if ($(window).scrollTop() < 100 && $('#header').data('positioned') == 'true') {
			$("#header").removeClass('fix').removeClass('nav-down').data('positioned', 'false');
		}
	}
	$(window).scroll(showDiv);
	$('#header').data('positioned', 'false');

	//input password change type on fill
	$('.pass-inp').focus(function(){     
	    $(this).attr('type', 'password'); 
	});
	$('.pass-inp').change(function(){     
	    if( $(this).val().length==0 ){
		    $(this).attr('type', 'text');
		}
		else{
		    $(this).attr('type', 'password'); 
			$('.eye input').change(function() {
			  var isChecked = $(this).prop('checked');
			  if (isChecked) {
			    $(this).parent().prev('input').attr('type', 'text');
			    $(this).parent().addClass('active')
			  }
			  else {
			    $(this).parent().prev('input').attr('type', 'password'); 
			    $(this).parent().removeClass('active')
			  }
			});
		}
	});

	//custom scroll
	if ($('.scroll').length) {
		$('.scroll').mCustomScrollbar({
			axis:"y",
			scrollButtons:{enable:false},
			advanced:{autoExpandHorizontalScroll:true},
			scrollInertia: 0
		});
	};

	//
	$(window).load(function() {
		$('body').removeClass('loaded');
	});

	$('.styled').styler();

});

