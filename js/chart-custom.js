if($('#myChart').length){
var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        datasets: [{
            label: ' ',
            data: [9, 10, 11, 15, 16, 14, 13, 9, 7, 11, 16],
            backgroundColor: 'rgba(85, 187, 0, 0.2)',
            borderColor: '#55bb00',
            borderWidth: 1.5,
            pointRadius: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                display: false
            }]
        },
        legend: {
            display: false
        }
    }
});
};
//
if($('#myChart2').length){
var ctx = document.getElementById("myChart2");
var myChart2 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        datasets: [{
            label: ' ',
            data: [10, 12, 12, 13, 14, 15, 15, 15, 14, 13, 16],
            backgroundColor: 'rgba(85, 187, 0, 0.2)',
            borderColor: '#55bb00',
            borderWidth: 1.5,
            pointRadius: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                display: false
            }]
        },
        legend: {
            display: false
        }
    }
});
};
//
if($('#myChart3').length){
var ctx = document.getElementById("myChart3");
var myChart3 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        datasets: [{
            label: ' ',
            data: [9, 10, 11, 15, 16, 14, 13, 9, 7, 11, 16],
            backgroundColor: 'rgba(219, 55, 39, 0.2)',
            borderColor: '#db3727',
            borderWidth: 1.5,
            pointRadius: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                display: false
            }]
        },
        legend: {
            display: false
        }
    }
});
};
//
if($('#myChart4').length){
var ctx = document.getElementById("myChart4");
var myChart4 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        datasets: [{
            label: ' ',
            data: [17, 16, 16, 15, 15, 14, 14, 13, 13, 14, 14],
            backgroundColor: 'rgba(219, 55, 39, 0.2)',
            borderColor:                '#db3727',
            borderWidth: 1.5,
            pointRadius: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                display: false
            }]
        },
        legend: {
            display: false
        }
    }
});
};
//
//
if($('#myChart5').length){
var ctx = document.getElementById("myChart5");
var myChart5 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        datasets: [{
            label: ' ',
            data: [15, 10, 7, 8, 5, 14, 16, 9, 7, 11, 16],
            backgroundColor: 'rgba(85, 187, 0, 0.2)',
            borderColor:'#55bb00',
            borderWidth: 1.5,
            pointRadius: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                display: false
            }]
        },
        legend: {
            display: false
        }
    }
});
};
//
var ctx = document.getElementById("chartBig1");
var chartBig1 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["7 авг", "8 авг", "9 авг", "10 авг", "11 авг", "12 авг", "13 авг"],
        datasets: [{
            label: ' ',
            data: [6100, 6000, 6045, 5600, 3500, 5000, 5900],
            borderColor: '#ae2231',
            borderWidth: 2,
            pointBackgroundColor : '#ae2231',
            fill: false,
            pointRadius: 3,            
            pointHoverRadius: 6,  

        }]
    },
    options: {
        
        scales: {
        	yAxes: [{
                stacked: true,
                ticks: {
	                fontSize: 12,
	                beginAtZero: true,
                    steps: 5,
                    stepSize: 2000,
                    max: 10000,
                    
	            },
             
            }],
            xAxes : [{
	            gridLines : {
	                display : false
	            },
                ticks: {
	                fontSize: 12
	            }
	        }]
        },
        legend: {
            display: false
        },
        tooltips: {
          	enabled:true,         	
          	yPadding: 10,
         	xPadding:20,
         	callbacks: {
                label: function(tooltipItems, data) {
                    return tooltipItems.yLabel;
                }
            },
            titleFontStyle:"normal",
            titleFontSize:13,
            bodyFontSize: 13,
            bodyFontStyle:"bold",
        },
    }
});
//
var ctx = document.getElementById("chartBig2");
var chartBig2 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["7 авг", "8 авг", "9 авг", "10 авг", "11 авг", "12 авг", "13 авг"],
        datasets: [{
            label: ' ',
            data: [6100, 6000, 6045, 5600, 3500, 5000, 5900],
            borderColor: '#ae2231',
            borderWidth: 2,
            pointBackgroundColor : '#ae2231',
            fill: false,
            pointRadius: 3,            
            pointHoverRadius: 6,  
           
        }],
        
    },
    options: {
        scales: {
        	yAxes: [{
                stacked: true,
                ticks: {
	                fontSize: 12,
	                beginAtZero: true,
                    steps: 5,
                    stepSize: 2000,
                    max: 10000
	            }
            }],
            xAxes : [{
	            gridLines : {
	                display : false
	            },
                ticks: {
	                fontSize: 12
	            }
	        }], 
        },
        legend: {
            display: false
        },
        tooltips: {
          	enabled:true,         	
          	yPadding: 10,
         	xPadding:20,
         	callbacks: {
                label: function(tooltipItems, data) {
                    return tooltipItems.yLabel;
                }
            },
            titleFontStyle:"normal",
            titleFontSize:13,
            bodyFontSize: 13,
            bodyFontStyle:"bold",
        },
    }
});

//
if($('#myChart6').length){
var ctx = document.getElementById("myChart6");
var myChart6 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        datasets: [{
            label: ' ',
            data: [9, 10, 11, 15, 16, 14, 13, 9, 7, 11, 16],
            backgroundColor: 'rgba(85, 187, 0, 0.2)',
            borderColor:  '#55bb00',
            borderWidth: 1.5,
            pointRadius: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                display: false
            }]
        },
        legend: {
            display: false
        }
    }
});
};
//
if($('#myChart7').length){
var ctx = document.getElementById("myChart7");
var myChart7 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        datasets: [{
            label: ' ',
            data: [10, 12, 12, 13, 14, 15, 15, 15, 14, 13, 16],
            backgroundColor: 'rgba(85, 187, 0, 0.2)',
            borderColor: '#55bb00',
            borderWidth: 1.5,
            pointRadius: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                display: false
            }]
        },
        legend: {
            display: false
        }
    }
});
};
//
if($('#myChart8').length){
var ctx = document.getElementById("myChart8");
var myChart8 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        datasets: [{
            label: ' ',
            data: [9, 10, 11, 15, 16, 14, 13, 9, 7, 11, 16],
            backgroundColor: 'rgba(219, 55, 39, 0.2)',
            borderColor:                '#db3727'
            ,
            borderWidth: 1.5,
            pointRadius: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                display: false
            }]
        },
        legend: {
            display: false
        }
    }
});
};
//
if($('#myChart9').length){
var ctx = document.getElementById("myChart9");
var myChart9 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        datasets: [{
            label: ' ',
            data: [17, 16, 16, 15, 15, 14, 14, 13, 13, 14, 14],
            backgroundColor: 'rgba(219, 55, 39, 0.2)',
            borderColor: '#db3727',
            borderWidth: 1.5,
            pointRadius: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                display: false
            }]
        },
        legend: {
            display: false
        }
    }
});
};
//
//
if($('#myChart10').length){
var ctx = document.getElementById("myChart10");
var myChart10 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        datasets: [{
            label: ' ',
            data: [15, 10, 7, 8, 5, 14, 16, 9, 7, 11, 16],
            backgroundColor: 'rgba(85, 187, 0, 0.2)',
            borderColor:  '#55bb00',
            borderWidth: 1.5,
            pointRadius: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                display: false
            }]
        },
        legend: {
            display: false
        }
    }
});
};
//
if($('#myChart11').length){
var ctx = document.getElementById("myChart11");
var myChart11 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        datasets: [{
            label: ' ',
            data: [9, 10, 11, 15, 16, 14, 13, 9, 7, 11, 16],
            backgroundColor:  'rgba(85, 187, 0, 0.2)',
            borderColor:'#55bb00',
            borderWidth: 1.5,
            pointRadius: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                display: false
            }]
        },
        legend: {
            display: false
        }
    }
});
};
//
if($('#myChart12').length){
var ctx = document.getElementById("myChart12");
var myChart12 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        datasets: [{
            label: ' ',
            data: [10, 12, 12, 13, 14, 15, 15, 15, 14, 13, 16],
            backgroundColor: 'rgba(85, 187, 0, 0.2)'
            ,
            borderColor: '#55bb00'
            ,
            borderWidth: 1.5,
            pointRadius: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                display: false
            }]
        },
        legend: {
            display: false
        }
    }
});
};
//
if($('#myChart13').length){
var ctx = document.getElementById("myChart13");
var myChart13 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        datasets: [{
            label: ' ',
            data: [9, 10, 11, 15, 16, 14, 13, 9, 7, 11, 16],
            backgroundColor:'rgba(219, 55, 39, 0.2)'
            ,
            borderColor: '#db3727'
            ,
            borderWidth: 1.5,
            pointRadius: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                display: false
            }]
        },
        legend: {
            display: false
        }
    }
});
};
//
if($('#myChart14').length){
var ctx = document.getElementById("myChart14");
var myChart14 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        datasets: [{
            label: ' ',
            data: [17, 16, 16, 15, 15, 14, 14, 13, 13, 14, 14],
            backgroundColor: 'rgba(219, 55, 39, 0.2)'
            ,
            borderColor: '#db3727'
            ,
            borderWidth: 1.5,
            pointRadius: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                display: false
            }]
        },
        legend: {
            display: false
        }
    }
});
};
//
//
if($('#myChart15').length){
var ctx = document.getElementById("myChart15");
var myChart15 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        datasets: [{
            label: ' ',
            data: [15, 10, 7, 8, 5, 14, 16, 9, 7, 11, 16],
            backgroundColor: 'rgba(85, 187, 0, 0.2)'
            ,
            borderColor: '#55bb00'
            ,
            borderWidth: 1.5,
            pointRadius: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                display: false
            }]
        },
        legend: {
            display: false
        }
    }
});
};
if($('#myChart16').length){
var ctx = document.getElementById("myChart16");
var myChart16 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        datasets: [{
            label: ' ',
            data: [9, 10, 11, 15, 16, 14, 13, 9, 7, 11, 16],
            backgroundColor: 'rgba(85, 187, 0, 0.2)'
            ,
            borderColor: '#55bb00'
            ,
            borderWidth: 1.5,
            pointRadius: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                display: false
            }]
        },
        legend: {
            display: false
        }
    }
});
};
if($('#myChart17').length){
var ctx = document.getElementById("myChart17");
var myChart17 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        datasets: [{
            label: ' ',
            data: [10, 12, 12, 13, 14, 15, 15, 15, 14, 13, 16],
            backgroundColor: 'rgba(85, 187, 0, 0.2)'            ,
            borderColor: '#55bb00'            ,
            borderWidth: 1.5,
            pointRadius: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                display: false
            }]
        },
        legend: {
            display: false
        }
    }
});
};
if($('#myChart18').length){
var ctx = document.getElementById("myChart18");
var myChart18 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        datasets: [{
            label: ' ',
            data: [9, 10, 11, 15, 16, 14, 13, 9, 7, 11, 16],
            backgroundColor: 'rgba(219, 55, 39, 0.2)'            ,
            borderColor:'#db3727'            ,
            borderWidth: 1.5,
            pointRadius: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                display: false
            }]
        },
        legend: {
            display: false
        }
    }
});
};
if($('#myChart19').length){
var ctx = document.getElementById("myChart19");
var myChart19 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        datasets: [{
            label: ' ',
            data: [17, 16, 16, 15, 15, 14, 14, 13, 13, 14, 14],
            backgroundColor:'rgba(219, 55, 39, 0.2)'            ,
            borderColor: '#db3727'            ,
            borderWidth: 1.5,
            pointRadius: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                display: false
            }]
        },
        legend: {
            display: false
        }
    }
});
};
if($('#myChart20').length){
var ctx = document.getElementById("myChart20");
var myChart20 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        datasets: [{
            label: ' ',
            data: [15, 10, 7, 8, 5, 14, 16, 9, 7, 11, 16],
            backgroundColor:  'rgba(85, 187, 0, 0.2)'
            ,
            borderColor: '#55bb00'
            ,
            borderWidth: 1.5,
            pointRadius: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                display: false
            }]
        },
        legend: {
            display: false
        }
    }
});
};

//helgraf1
if($('#helgraf1').length){
var ctx = document.getElementById("helgraf1");
var helgraf1 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        datasets: [{
            label: ' ',
            data: [9, 10, 11, 15, 16, 14, 13, 9, 7, 11, 16],
            backgroundColor: 'rgba(246, 226, 122, 0.5)'
            ,
            borderColor: '#f6e27a'
            ,
            borderWidth: 1.5,
            pointRadius: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                display: false
            }]
        },
        legend: {
            display: false
        }
    }
});
};
//helgraf1
if($('#helgraf2').length){
var ctx = document.getElementById("helgraf2");
var helgraf2 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        datasets: [{
            label: ' ',
            data: [10, 12, 12, 13, 14, 15, 15, 15, 14, 13, 16],
            backgroundColor: 'rgba(246, 226, 122, 0.5)'
            ,
            borderColor: '#f6e27a'
            ,
            borderWidth: 1.5,
            pointRadius: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                display: false
            }]
        },
        legend: {
            display: false
        }
    }
});
};
//helgraf3
if($('#helgraf3').length){
var ctx = document.getElementById("helgraf3");
var helgraf3 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        datasets: [{
            label: ' ',
            data: [9, 10, 11, 15, 16, 14, 13, 9, 7, 11, 16],
            backgroundColor: 'rgba(246, 226, 122, 0.5)'
            ,
            borderColor: '#f6e27a'
            ,
            borderWidth: 1.5,
            pointRadius: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                display: false
            }]
        },
        legend: {
            display: false
        }
    }
});
};
//helgraf4
if($('#helgraf4').length){
var ctx = document.getElementById("helgraf4");
var helgraf4 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
        datasets: [{
            label: ' ',
            data: [17, 16, 16, 15, 15, 14, 14, 13, 13, 14, 14],
            backgroundColor: 'rgba(246, 226, 122, 0.5)'
            ,
            borderColor: '#f6e27a'
            ,
            borderWidth: 1.5,
            pointRadius: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                display: false
            }]
        },
        legend: {
            display: false
        }
    }
});
};

///////////////
//
var ctx = document.getElementById("chartBig1_2");
var chartBig1 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["7 авг", "8 авг", "9 авг", "10 авг", "11 авг", "12 авг", "13 авг"],
        datasets: [{
            label: ' ',
            data: [6100, 6000, 6045, 5600, 3500, 5000, 5900],
            borderColor: '#ae2231',
            borderWidth: 2,
            pointBackgroundColor : '#ae2231',
            fill: false,
            pointRadius: 3,            
            pointHoverRadius: 6,  

        }]
    },
    options: {
        
        scales: {
            yAxes: [{
                stacked: true,
                ticks: {
                    fontSize: 12,
                    beginAtZero: true,
                    steps: 5,
                    stepSize: 2000,
                    max: 10000,
                    
                },
             
            }],
            xAxes : [{
                gridLines : {
                    display : false
                },
                ticks: {
                    fontSize: 12
                }
            }]
        },
        legend: {
            display: false
        },
        tooltips: {
            enabled:true,           
            yPadding: 10,
            xPadding:20,
            callbacks: {
                label: function(tooltipItems, data) {
                    return tooltipItems.yLabel;
                }
            },
            titleFontStyle:"normal",
            titleFontSize:13,
            bodyFontSize: 13,
            bodyFontStyle:"bold",
        },
    }
});
//
var ctx = document.getElementById("chartBig1_3");
var chartBig1 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["7 авг", "8 авг", "9 авг", "10 авг", "11 авг", "12 авг", "13 авг"],
        datasets: [{
            label: ' ',
            data: [6100, 6000, 6045, 5600, 3500, 5000, 5900],
            borderColor: '#ae2231',
            borderWidth: 2,
            pointBackgroundColor : '#ae2231',
            fill: false,
            pointRadius: 3,            
            pointHoverRadius: 6,  

        }]
    },
    options: {
        
        scales: {
            yAxes: [{
                stacked: true,
                ticks: {
                    fontSize: 12,
                    beginAtZero: true,
                    steps: 5,
                    stepSize: 2000,
                    max: 10000,
                    
                },
             
            }],
            xAxes : [{
                gridLines : {
                    display : false
                },
                ticks: {
                    fontSize: 12
                }
            }]
        },
        legend: {
            display: false
        },
        tooltips: {
            enabled:true,           
            yPadding: 10,
            xPadding:20,
            callbacks: {
                label: function(tooltipItems, data) {
                    return tooltipItems.yLabel;
                }
            },
            titleFontStyle:"normal",
            titleFontSize:13,
            bodyFontSize: 13,
            bodyFontStyle:"bold",
        },
    }
});